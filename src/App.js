import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Step1 from './components/Step1'
import Step2 from './components/Step2'
import Step3 from './components/Step3'
import Review from './components/Review'

class App extends Component {

  state = {
    step: 1,
    orderForm: {
      meal:             '',
      numberOfPeople:   1,
      restaurant:       '',
      dishes: [{        dish: '',
                        numberOfServings: 1
      }]
    },
    isFormValid: false
  }

  nextStep = () => {
    this.setState({
      step: this.state.step + 1
    });
  }

  previousStep = () => {
    this.setState({
      step: this.state.step - 1
    });
  }

  updateOrderForm = (formData) => {
    let orderForm = this.state.orderForm;
    orderForm = Object.assign(orderForm, formData);
    console.log(this.state.orderForm);
  }

  updateIsFormValid = (isFormValid) => {
    this.setState({
      isFormValid: isFormValid
    });
  }

  handleSubmit = () => {
    console.log(JSON.stringify({"Order form": [this.state.orderForm]}, null, 2));
  }

  showStep() {
    switch (this.state.step) {
      case 1:
        return <Step1 orderForm={this.state.orderForm}
                      updateOrderForm={this.updateOrderForm}
                      nextStep={this.nextStep}
                      isFormValid={this.state.isFormValid}
                      updateIsFormValid={this.updateIsFormValid} />;
      case 2:
        return <Step2 orderForm={this.state.orderForm}
                      updateOrderForm={this.updateOrderForm}
                      previousStep={this.previousStep}
                      nextStep={this.nextStep}
                      isFormValid={this.state.isFormValid}
                      updateIsFormValid={this.updateIsFormValid}  />;
      case 3:
        return <Step3 orderForm={this.state.orderForm}
                      updateOrderForm={this.updateOrderForm}
                      previousStep={this.previousStep}
                      nextStep={this.nextStep}
                      isFormValid={this.state.isFormValid}
                      updateIsFormValid={this.updateIsFormValid}  />;
      case 4:
        return <Review orderForm={this.state.orderForm}
                      previousStep={this.previousStep}
                      handleSubmit={this.handleSubmit}
                      isFormValid={this.state.isFormValid}
                      updateIsFormValid={this.updateIsFormValid}  />;
    }
  }

  render() {
    return(
      this.showStep()
    )
  }
}

export default App;
