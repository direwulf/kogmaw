import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import data from '../dishes.json';

class Step3 extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dishes: this.props.orderForm.dishes,
      maxNumberOfPeople: 10,
      isFormValid: this.props.isFormValid,
    }
  }

  dishOptions(selectedDish) {
    let temp = [];
    let names = [];
    let options = [];
    const dishes = data.dishes;

    for(let i = 0; i < dishes.length; i++) {
      let dish = dishes[i];
      for (let i = 0; i < dish.availableMeals.length; i++) {
        let availableMeal = dish.availableMeals[i];
        if(availableMeal==this.props.orderForm.meal) {
          temp.push(dish);
        }
      }
    }

    if(temp.length > 1) {
      for(let i = 0; i < temp.length; i++) {
        let dish = temp[i];
        if(dish.restaurant == this.props.orderForm.restaurant) {
          names.push(dish.name);
        }
      }
    } else {
      names.push(temp[0].name);
    }

    if(names.length > 1) {
      for(let i = 0; i < names.length; i++) {
        options.push(<option key={i} value={names[i]}>{names[i]}</option>)
      }
    } else {
      options.push(<option key={0} value={temp[0].name}>{temp[0].name}</option>);
    }

    return options;
  }

  createDishInputs() {
    const dishes = this.state.dishes.map((object, i) =>
      <li key={i}>
        <select value={object.dish} onChange={this.handleDishChange.bind(this, i)}>
          <option value="">Please select</option>
          {this.dishOptions(object)}
        </select>
        <br />
        <input
          name="numberOfServings"
          ref="numberOfServings"
          type="number"
          min="1"
          max="10"
          value={object.numberOfServings}
          onChange={this.handleNumberOfServingsChange.bind(this, i)}
          />
        {this.renderRemoveDishInputButton()}
      </li>
    )
    return dishes;
  }

  validateDishInput(value) {
    const error = document.getElementById('formError');

    if (value === "") {
      error.textContent = 'Please select a dish';
      this.setState({
        isFormValid: false
      })
      return false
    }

    error.textContent = '';
    this.setState({
      isFormValid: true
    })
    return true
  }

  validateNumberOfServingsInput() {
     const validity = this.refs.numberOfServings.validity;
     const error = document.getElementById('formError');

     if (!validity.valid || validity.valueMissing || validity.underOverflow || validity.rangeOverflow) {
       error.textContent = `Please enter a number from 1 to 10`;
       return false;
     }

     error.textContent = '';
     this.setState({
       isFormValid: true
     })
     return true;
  }

  renderRemoveDishInputButton() {
    const dishesNumber = this.state.dishes.length;
    let isDisabled = false;
    if( dishesNumber <= 1) {
      isDisabled = true;
    }
    return <button disabled={isDisabled} onClick={this.removeDishInput.bind(this)}>Remove</button>
  }

  renderAddDishInputButton(i) {
    const dishesNumber = this.state.dishes.length - 1;
    const optionsNumber = this.dishOptions().length;
    let isButtonInvalid = false;
    if(optionsNumber == dishesNumber + 1) {
      isButtonInvalid = true;
    }
    return <button disabled={isButtonInvalid} onClick={this.addDishInput.bind(this)}>Add</button>
  }

  getSelectedDishesNames() {
    let names = [];
    for (let i = 0; i < this.state.dishes.length; i ++) {
      names.push(this.state.dishes[i].dish)
    }
    return names;
  }

  addDishInput() {
    this.setState(prevState => ({ dishes: [...prevState.dishes, {dish: "", numberOfServings: 1}]}));
  }

  removeDishInput(i) {
    let dishes = [...this.state.dishes];
    dishes.splice(i,1);
    this.setState({ dishes });
  }


  handleDishChange(i, event) {
    let dishes = [...this.state.dishes];
    dishes[i].dish = event.target.value;
    this.setState({dishes});

    this.validateDishInput(event.target.value);
  }

  handleNumberOfServingsChange(i, event) {
    let dishes = [...this.state.dishes];
    dishes[i].numberOfServings = event.target.value;
    this.setState({dishes});

    this.validateNumberOfServingsInput(event.target.value)
  }

  render() {

    return (
      <div>
        <ul>
          {this.createDishInputs()}
        </ul>
        <div className="error" id="formError" />
        {this.renderAddDishInputButton()}
        <button onClick={this.previousStep}>Previous</button>
        <button disabled={!this.state.isFormValid} onClick={this.nextStep}>Next</button>
      </div>
    );
  }

  previousStep = (e) => {
    e.preventDefault();
    this.props.updateOrderForm({dishes: [{dish: '', numberOfServings: 1}]})
    this.props.updateIsFormValid(true);
    this.props.previousStep();
  }

  nextStep = (e) => {
    e.preventDefault()
    this.props.updateOrderForm({dishes: this.state.dishes})
    this.props.updateIsFormValid(false);
    this.props.nextStep()
  }
}

export default Step3
