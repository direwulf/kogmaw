import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import data from '../dishes.json';

class Step2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      restaurant: this.props.orderForm.restaurant,
      isFormValid: this.props.isFormValid,
    }

    this.handleRestaurantChange = this.handleRestaurantChange.bind(this);
  }

  restaurantOptions() {
    let temp = [];
    let options = [];
    const dishes = data.dishes;

    for (let i = 0; i < dishes.length; i++) {
      let dish = dishes[i];
      for (let i = 0; i < dish.availableMeals.length; i++) {
        let availableMeal = dish.availableMeals[i];
        if(availableMeal==this.props.orderForm.meal) {
          temp.push(dish.restaurant);
        }
      }
    }

    let restaurants = [...new Set(temp)];

    for (let i = 0; i <= restaurants.length - 1; i++) {
      options.push(<option key={i} value={restaurants[i]}>{restaurants[i]}</option>)
    }

    return options;
  }

  handleRestaurantChange(e) {
    this.setState({
      restaurant: e.target.value
    });

    this.validateRestaurantInput(e.target.value);
  }

  validateRestaurantInput(value) {
    const error = document.getElementById('restaurantError');

    if (value === "") {
      error.textContent = 'Please select a restaurant';
      this.setState({
        isFormValid: false
      })
      return false
    }

    error.textContent = '';
    this.setState({
      isFormValid: true
    })
    return true
  }

  render() {
    this.restaurantOptions();
    return(
      <div>
        <label>
          Select a restaurant
          <br />
          <select value={this.state.restaurant}
                  onChange={this.handleRestaurantChange}
                  ref="restaurant"
                  name="restaurant" >
            <option value="">Please select</option>
            {this.restaurantOptions()}
          </select>
          <br />
          <div className="error" id="restaurantError" />
        </label>
        <div className="actions">
          <button onClick={this.previousStep}>Previous</button>
          <button disabled={!this.state.isFormValid} onClick={this.nextStep}>Next</button>
        </div>
      </div>
    );
  }


  previousStep = (e) => {
    e.preventDefault();
    this.props.updateOrderForm({restaurant: ""});
    this.props.updateIsFormValid(true);
    this.props.previousStep();
  }

  nextStep = (e) => {
    e.preventDefault();
    this.props.updateOrderForm({restaurant: this.state.restaurant});
    this.props.updateIsFormValid(false);
    this.props.nextStep();
  }
}

export default Step2
