import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Step1 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      meal: this.props.orderForm.meal,
      numberOfPeople: this.props.orderForm.numberOfPeople,
      maxNumberOfPeople: 10,
      isFormValid: this.props.isFormValid,
    };

    this.handleMealChange = this.handleMealChange.bind(this);
    this.handleNumberOfPeopleChange = this.handleNumberOfPeopleChange.bind(this);
  }

  handleMealChange(e) {
    this.setState({
      meal: e.target.value
    });

    this.validateMealInput(e.target.value);
  }

  handleNumberOfPeopleChange(e) {
    this.setState({
      numberOfPeople: e.target.value
    });
  }

  validateMealInput(value) {
    const error = document.getElementById('mealError');

    if (value === "") {
      error.textContent = 'Please select a meal';
      this.setState({
        isFormValid: false
      })
      return false
    }

    error.textContent = '';
    this.setState({
      isFormValid: true
    })
    return true
  }

 numberOfPeopleOptions() {
   const options = [];
   for (let i = 1; i <= 10; i++) {
     options.push(<option key={i} value={i}>{i}</option>)
   }
   return options;
  }

  render() {
    return (
      <div>
        <label>
          Select a meal
          <br />
          <select value={this.state.meal}
                  onChange={this.handleMealChange}
                  ref="meal"
                  name="meal" >
            <option value="">Please select</option>
            <option value="breakfast">Breakfast</option>
            <option value="lunch">Lunch</option>
            <option value="dinner">Dinner</option>
          </select>
          <br />
          <div className="error" id="mealError" />
        </label>
        <br />
        <label>
          Number of people
          <br />
          <select value={this.state.numberOfPeople}
                  onChange={this.handleNumberOfPeopleChange}
                  ref="numberOfPeople"
                  name="numberOfPeople" >
            {this.numberOfPeopleOptions()}
          </select>
        </label>
        <div className="actions">
          <button disabled={!this.state.isFormValid} onClick={this.nextStep}>Next</button>
        </div>
      </div>
    );
  }

  nextStep = (e) => {
    e.preventDefault();
    this.props.updateOrderForm({meal: this.state.meal, numberOfPeople: this.state.numberOfPeople});
    this.props.updateIsFormValid(false);
    this.props.nextStep();
  }
}

export default Step1
